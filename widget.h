#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QNetworkAccessManager>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pushButton_clicked();
    void httpFinished();
    void sslErrors(const QList<QSslError> &errors);

private:
    Ui::Widget *ui;
    QNetworkAccessManager *qnam;
    QNetworkReply *reply;
};
#endif // WIDGET_H
