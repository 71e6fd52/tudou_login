#include "widget.h"
#include "ui_widget.h"
#include <QUrlQuery>
#include <QNetworkReply>
#include <QMessageBox>
#include <QNetworkCookieJar>
#include <QNetworkCookie>
#include <QDateTime>

#include <Windows.h>
#include <wininet.h>


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , reply(nullptr)
{
    ui->setupUi(this);
    qnam = new QNetworkAccessManager(this);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    if (reply != nullptr) {
        return;
    }
    QUrlQuery postData;
    postData.addQueryItem("username", ui->username->text());
    postData.addQueryItem("password", ui->password->text());
    postData.addQueryItem("templateid", "0");
    postData.addQueryItem("login", "");
    postData.addQueryItem("expires", "43200");

    QNetworkRequest request(QUrl("https://www.keyfc.net/bbs/login.aspx"));
    request.setHeader(QNetworkRequest::ContentTypeHeader,
        "application/x-www-form-urlencoded");
    reply = qnam->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());

    connect(reply, &QNetworkReply::finished, this, &Widget::httpFinished);
    connect(reply, &QNetworkReply::sslErrors,
            this, &Widget::sslErrors);

    ui->log->setText("正在登录");
    ui->pushButton->setEnabled(false);
}

void Widget::httpFinished()
{
    QNetworkReply::NetworkError error = reply->error();
    const QString &errorString = reply->errorString();
    if (error != QNetworkReply::NoError) {
        ui->log->setText(QString("登录失败：%1。").arg(errorString));
        ui->pushButton->setEnabled(true);
        reply = nullptr;
        return;
    }
    QByteArray value;
    for(const auto &i : qnam->cookieJar()->cookiesForUrl(QUrl("https://www.keyfc.net/"))) {
        if(i.name() == "dnt") {
            value = i.value();
            break;
        }
    }
    if(value == "") {
        ui->log->setText("登录失败");
        ui->pushButton->setEnabled(true);
        reply = nullptr;
        return;
    }

    ui->log->setText("正在写入 Cookie");
    value.append(";Expires=");
    auto date = QDateTime::currentDateTimeUtc().addMonths(1).toString("ddd, dd MMM yyyy hh:mm:ss GMT");
    value.append(date.toUtf8());

    auto res = InternetSetCookieA("http://www.keyfc.net", "dnt", value);
    if (!res)
    {
        ui->log->setText(QString("Cookie 写入失败：%1。").arg(GetLastError()));
    }else{
        ui->log->setText("完成！");

        if (QMessageBox::information(this, "完成",
                                 "成功写入 Cookie") == QMessageBox::Ok) {
            close();
        }
    }
}

void Widget::sslErrors(const QList<QSslError> &errors)
{
    QString errorString;
    for (const QSslError &error : errors) {
        if (!errorString.isEmpty())
            errorString += '\n';
        errorString += error.errorString();
    }

    if (QMessageBox::warning(this, "SSL 错误",
                             QString("SSL 连接错误:\n%1").arg(errorString),
                             QMessageBox::Ignore | QMessageBox::Abort) == QMessageBox::Ignore) {
        reply->ignoreSslErrors();
    }
}
